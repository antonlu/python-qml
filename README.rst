=================
Q Markup Language
=================

A parser for a simple markup language. In analogy to Python YAML.

Detailed documentation is in the "docs" directory.

Examples coming soon.

