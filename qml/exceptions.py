class IsComment(Exception):
    """Raised when the line is a comment"""
    pass